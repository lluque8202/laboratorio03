package com.example.appmascotas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundleDestino = intent.extras

        bundleDestino?.let {
            val nombre = it.getString("key_nombres", "") ?: ""
            val edad = it.getString("key_edad","") ?: ""
            val tipo = it.getString("key_tipo","perro") ?: ""
            val vacuna = it.getString("key_vacuna","") ?: ""

            when(tipo){
                "Perro"-> ivMascota.setImageResource(R.drawable.icono_perro)
                "Gato" -> ivMascota.setImageResource(R.drawable.icono_gato)
                "Conejo"->ivMascota.setImageResource(R.drawable.icono_conejo)
            }
            tvTipo.text = "$tipo"
            tvNombre.text = "$nombre"
            tvEdad.text = "$edad"
            tvVacunas.text = "$vacuna"
            btCerrar.setOnClickListener {
                finish()
            }
        }
    }
}